#use wml::debian::translation-check translation="5357ead7bb298e3857977fea7b0087543c6072b3" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i the webbläsaren chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5787">CVE-2019-5787</a>

    <p>Zhe Jin upptäckte ett problem med användning efter frigörning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5788">CVE-2019-5788</a>

    <p>Mark Brand upptäckte ett problem med användning efter frigörning i implementationen
	av FileAPI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5789">CVE-2019-5789</a>

    <p>Mark Brand upptäckte ett problem med användning efter frigörning i implementationen
	av WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5790">CVE-2019-5790</a>

    <p>Dimitri Fourny upptäckte ett buffertspill i javaskriptbiblioteket v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5791">CVE-2019-5791</a>

    <p>Choongwoo Han upptäckte ett typförvirringsproblem i javaskriptbiblioteket v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5792">CVE-2019-5792</a>

    <p>pdknsk upptäckte ett problem med heltalsspill i biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5793">CVE-2019-5793</a>

    <p>Jun Kokatsu upptäckte ett rättighetsfel i implementationen av tillägg.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5794">CVE-2019-5794</a>

    <p>Juno Im of Theori upptäckte ett problem med förfalskning av användargränssnittet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5795">CVE-2019-5795</a>

    <p>pdknsk upptäckte ett problem med heltalsspill i biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5796">CVE-2019-5796</a>

    <p>Mark Brand upptäckte en kapplöpningseffekt i implementationen av tillägg.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5797">CVE-2019-5797</a>

    <p>Mark Brand upptäckte en kapplöpningseffekt i implementationen av DOMStorage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5798">CVE-2019-5798</a>

    <p>Tran Tien Hung upptäckte en läsning utanför avgränsningarna i biblioteket skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5799">CVE-2019-5799</a>

    <p>sohalt upptäckte ett sätt att förbigå Content Security Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5800">CVE-2019-5800</a>

    <p>Jun Kokatsu upptäckte ett sätt att förbigå Content Security Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5802">CVE-2019-5802</a>

    <p>Ronni Skansing upptäckte ett problem med förfalskning av användargränssnittet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5803">CVE-2019-5803</a>

    <p>Andrew Comminos upptäckte ett sätt att förbigå Content Security Policy.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 73.0.3683.75-1~deb9u1.</p>

<p>Vi rekommenderar att ni uppgraderar era chromium-paket.</p>

<p>För detaljerad säkerhetsstatus om chromium vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/chromium">https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4421.data"
