#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986"
<define-tag pagetitle>Debian 10 <q>buster</q> udgivet</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news

<p>Efter 25 måneders udvikling, er Debian-projektet stolt over at kunne 
præsentere sin nye stabile version 10 (kodenavn <q>buster</q>), som vil blive 
understøttet de næste fem år, takket være det samlede arbejde, der udføres af
<a href="https://security-team.debian.org/">Debian Security-holdet</a> og
<a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>-holdet.</p>

<p>Debian 10 <q>buster</q> leverer med flere skrivebordsapplikationer og 
-miljøer.  Der er blandt andre tale om skrivebordsmiljøerne:</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>I denne udgave benytter GNOME som standard skærmserveren Wayland, i stedet 
for Xorg.  Wayland har et simplere og mere moderne design, som giver 
sikkerhedsfordele.  Dog installeres skærmserveren Xorg stadig som standard, 
og standardskærmvælgeren giver brugerne mulighed for at vælge Xorg som 
skærmserver i deres næste session.</p>

<p>Takket være projektet Reproducible Builds, vil over 91 procent af 
kildekodepakkerne, som følger med Debian 10, blive opbygget identisk 
bit for bit med de binære pakker.  Det er en vigtig kontrolfunktion, som 
benytter brugerne mod ondsindede forsøg på at manipulere med compilere og 
buildnetværk.  Fremtidige Debian-udgaver vil indeholde værktøjer og metadata, 
så slutbrugere kan validere ægtheden af pakker i arkivet.</p>

<p>Til brug i sikkerhedsfølsomme miljøer, er AppArmor, et obligatorisk 
adgangskontrolframework til begrænsning af programmers muligheder, installeret 
aktiveret som standard.  Desuden kan alle metoder, der leveres af APT (bortset 
fra cdrom, gpgv og rsh) valgtfrit benytte <q>seccomp-BPF</q>-sandkasser.  APT's 
https-metode medfølger apt-pakken, og skal ikke installeres separat.</p>

<p>Netværksfiltrering er i Debian 10 <q>buster</q> som standard baseret på 
nftables-frameworket.  Begyndende med iptables v1.8.2, indeholder den binære 
pakke iptables-nft og iptables-legacy, to varianter af iptables' 
kommandolinjegrænseflade.  Den nftables-baserede variant anvender Linux 
kerne-undersystemet the nf_tables.  <q>alternatives</q>-systemet kan benyttes 
til at vælge mellem varianterne.</p>

<p>UEFI-understøttelsen (<q>Unified Extensible Firmware Interface</q>), som blev 
indført i Debian 7 (kodenavn <q>wheezy</q>) er blevet yderligere, kraftigt 
forbedret i Debian 10 <q>buster</q>.  Secure Boot-understøttelse fungerer i 
denne udgave med arkitekturerne amd64, i386 og arm64, og bør uden videre fungere 
på de fleste maskiner, hvor Secure Boot er aktiveret.  Det betyder, at brugerne 
ikke længere behøver at deaktivere Secure Boot-understøttelse i 
firmwareopsætningen.</p>

<p>Pakkerne cups og cups-filters installeres som standard i Debian 10 
<q>buster</q>, hvilket giver brugerne alt hvad der er behov for, for at kunne 
anvende driverløs udskrivning.  Netværksudskriftskøer og IPP-printere, bliver 
autoamtisk opsat og håndteret af cups-browsed, og anvendelse af ikke-frie 
leverandørers printerdrivere og -plugins, er ikke længere nødvendige.</p>

<p>Debian 10 <q>buster</q> indeholder mange opdaterede softwarepakker (over
62 procent af alle pakker i forhold til den foregående udgave), så som:</p>

<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (i pakken firefox-esr)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 og 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19 series</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>flere end 59.000 andre softwarepakker er klar til brug, opbygget fra næsten 
29.000 kildekodepakker.</li>
</ul>

<p>Med det brede udvalg af pakker og den traditionelle brede understøttelse af 
arkitekturer, lever Debian endnu en gang op til sin målsætning om at være det 
universelle styresystem.  Det er velegnet til mange forskellige formål: fra 
skriverbordssystemer til netbooks; fra udviklingservere til klyngesystemer; og 
til database-, web og storageservere.  På samme tid, sørger yderligere 
kvalitetssikringtiltag så som automatisk installerings- og opgraderestest af 
alle pakker i Debian arkiv, at <q>buster</q> opfylder de høje forventninger, 
som brugerne har til en stabil Debian-udgave.</p>

<p>I alt ti arkitekturer er understøttet:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
til ARM, <code>armel</code>
og <code>armhf</code> til ældre og nyere 32 bit-hardware,
samt <code>arm64</code> til 64 bit-arkitekturen <q>AArch64</q>,
og til MIPS, arkitekturerne <code>mips</code> (big-endian) 
og <code>mipsel</code> (little-endian) til 32 bit-hardware 
og 64 bit-arkitekturen <code>mips64el</code> til little-endian-hardware.
</p>

<h3>Hvad med en prøvetur?</h3>

<p>Hvis du blot ønsker at prøve Debian 10 <q>buster</q> uden at installere det, 
kan du benytte et af de tilgængelige <a href="$(HOME)/CD/live/">liveimages</a>, 
som indlæser og afvikler et komplet styresystem i kun læsning-tilstand ved hjælp 
af din computers hukommelse.</p>

<p>Disse liveimages findes til arkitekturerne <code>amd64</code> og
<code>i386</code>, og er tilgængelige til DVD'er, USB-pinde og 
netboot-opsætninger.  Brugeren kan afprøve forskellige skrivebordsmiljøer: 
Cinnamon, GNOME, KDE Plasma, LXDE, MATE, Xfce, og som en nyhed i buster, LXQt. 
Debian Live Buster genindfører standardliveimaget, så det også er muligt at 
prøve et grundlæggende Debian-system, uden en grafisk brugerflade.</p>

<p>Hvis du bliver glad for styresystemet, har du mulighed for at installere fra 
liveimaget til din computers harddisk.  Liveimaget indeholder det uafhængige 
installeringsprogram Calamares, samt den almindelige Debian Installer.  Flere 
oplysninger er tilgængelige i afsnittene 
<a href="$(HOME)/releases/buster/releasenotes">udgivelsesbemærkninger</a> og 
<a href="$(HOME)/CD/live/">liveinstalleringsimages</a> på Debian websted.</p>

<p>For at installere Debian 10 <q>buster</q> direkte på din computers harddisk, 
kan du vælge blandt forskellige medier, så som Blu-ray Disc, DVD, CD, USB-pind 
eller ved hjælp af en netværksforbindelse.  Flere skrivebordsmiljøer &ndash; 
Cinnamon, GNOME, KDE Plasma Desktop and Applications, LXDE, LXQt, MATE og Xfce 
&ndash; kan blive installeret fra disse images.  Desuden er der 
<q>multi-architecture</q>-CD'er, hvor man kan vælge installering blandt flere 
forskellige arkitekturer fra en enkelt skive.  Ellers kan du altid oprette et 
startbart USB-medie (se <a href="$(HOME)/releases/buster/installmanual">\
Installeringshåndbogen</a> for flere oplysninger).</p>

<p>Til skybrugere tilbyder Debian direkte understøttelse af mange de bedst 
kendte skyplatform.  Officielle Debian-images vælges let på hvert 
imagemarkedsplads.  Debian udgiver også 
<a href="https://cloud.debian.org/images/openstack/current/">forhåndsopbyggede
OpenStack-images</a> til arkitekturerne <code>amd64</code> og 
<code>arm64</code>, klar til hentning og brug i lokale skyopsætninger.</p>

<p>Debian kan nu installere på 76 sprog, hvoraf de fleste er tilgængelige i 
både tekst-baserede og grafisk brugergrænsflader.</p>

<p>Installeringsimages kan hentes lige nu ved hjælp af 
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (den anbefalede metode),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> eller 
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; se
<a href="$(HOME)/CD/">Debian på CD'er</a> for flere oplysninger. <q>Buster</q> 
bliver også snart tilgængelig på fysisk DVD, CD-ROM og Blu-ray Disc fra talrige 
<a href="$(HOME)/CD/vendors">forhandlere</a>.</p>


<h3>Opgradering af Debian</h3>

<p>Opgradering til Debian 10 fra den foregående version, Debian 9 (kodenavn 
<q>stretch</q>), håndteres automatisk af pakkehåndteringsværktøjet apt for de 
fleste opsætningers vedkommende.  Som altid kan Debian-systemer opgraderes 
smertefrit, på stedet og uden tvungen nedetid, men vi anbefaler kraftigt, at 
man læser 
<a href="$(HOME)/releases/buster/releasenotes">udgivelsesbemærkningerne</a> 
samt 
<a href="$(HOME)/releases/buster/installmanual">installeringsvejledningen</a>, 
for at være opmærksom på eventuelle problemer, samt for at få detaljeret 
vejledning i installering og opgradering.  I ugerne efter udgivelsen, vil 
udgivelsesbemærkningerne blive løbende forbedret og oversat til flere sprog.</p>


<h2>Om Debian</h2>

<p>Debian er et frit styresystem, der udvikles af tusindvis af frivillige fra 
hele verden, som samarbejder via internettet.  Debian-projektets hovedstyrker 
er dets fundament af frivillige, dets dedikation til Debians sociale kontrakt 
og fri software, samt dets engagement i at tilbyde det bedst mulige styresystem.  
Denne nye udgave er endnu et vigtigt skridt i den retning.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt;.</p>
