#use wml::debian::translation-check translation="a519393250458ed72be6cc6df911105563ae120c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans LibreOffice :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>

<p>Nils Emmerich a découvert que des documents malveillants pourraient
exécuter du code Python arbitraire grâce à LibreLogo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9849">CVE-2019-9849</a>

<p>Matei Badanoiu a découvert que le mode prudent ne s'appliquait pas aux
puces graphiques.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 1:5.2.7-1+deb9u9.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:6.1.5-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libreoffice.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libreoffice,
veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libreoffice">\
https://security-tracker.debian.org/tracker/libreoffice</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4483.data"
# $Id: $
