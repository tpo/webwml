#use wml::debian::translation-check translation="1ac8218b729e6fe62e8b52eb754400fe4a637557" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Jann Horn a découvert que le sous-système ptrace dans le noyau Linux ne
gère pas correctement les identifiants d'un processus qui veut créer une
relation ptrace, permettant à un utilisateur local d'obtenir les droits du
superutilisateur dans certains scénarios.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 4.9.168-1+deb9u4.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 4.19.37-5+deb10u1. Cette mise à jour comprend aussi une correction
pour une régression introduite par le correctif original pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a> (nº 930904).</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4484.data"
# $Id: $
