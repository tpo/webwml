<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The manager_dispatch_notify_fd function in systemd allowed local users to cause
a denial of service (system hang) via a zero-length message received over a
notify socket, which caused an error to be returned and the notification
handler to be disabled.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
44-11+deb7u5.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-659.data"
# $Id: $
