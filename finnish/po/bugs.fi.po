msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-05-16 10:41+0300\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "paketissa"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "tagilla"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "vakavuudella"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "lähdekoodipaketissa"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "paketissa, jota ylläpitää"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "joista on ilmoittanut"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "jotka omistaa"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "joissa on tila"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "joissa viestejä lähettäjältä"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "uusimmat viat"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "aihe sisältää"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "odottava tila"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "joissa ilmoittaja sisältää"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "jotka on välitetty eteenpäin osoitteeseen"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "joissa omistaja sisältää"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "paketissa"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normaali"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "vanha näkymä"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "raaka"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "ikä"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Toista yhdistetyt"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Käänteiset viat"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Käänteinen odottava tila"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Käänteinen vakavuus"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Ei vikoja, jotka liittyvät paketteihin"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "ei mitään"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testattava"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "aiempi vakaa"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "vakaa"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "kokeellinen"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "epävakaa"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "arkistoimattomat"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "arkistoidut"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "arkistoidut sekä arkistoimattomat"

#~ msgid "Exclude tag:"
#~ msgstr "Älä sisällytä määrettä: "

#~ msgid "Include tag:"
#~ msgstr "Sisällytä määre: "

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "wontfix"
#~ msgstr "ei korjata"

#~ msgid "upstream"
#~ msgstr "upstream"

#~ msgid "unreproducible"
#~ msgstr "ei toistettavissa"

#~ msgid "security"
#~ msgstr "tietoturva"

#~ msgid "patch"
#~ msgstr "paikkaus"

#~ msgid "moreinfo"
#~ msgstr "lisätietoja"

#~ msgid "l10n"
#~ msgstr "lokalisointi"

#~ msgid "help"
#~ msgstr "apua"

#~ msgid "fixed-upstream"
#~ msgstr "korjattu-upstream"

#~ msgid "fixed-in-experimental"
#~ msgstr "korjattu-experimentalissa"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "confirmed"
#~ msgstr "vahvistettu"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "lenny-ignore"
#~ msgstr "lenny-älävälitä"

#~ msgid "lenny"
#~ msgstr "lenny"

#~ msgid "etch-ignore"
#~ msgstr "etch-älävälitä"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-älävälitä"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "Exclude severity:"
#~ msgstr "Älä sisällytä vakavuutta: "

#~ msgid "Include severity:"
#~ msgstr "Sisällytä vakavuus: "

#~ msgid "wishlist"
#~ msgstr "toive"

#~ msgid "minor"
#~ msgstr "vähäpätöinen"

#~ msgid "important"
#~ msgstr "tärkeä"

#~ msgid "serious"
#~ msgstr "vakava"

#~ msgid "grave"
#~ msgstr "haudanvakava"

#~ msgid "critical"
#~ msgstr "kriittinen"

#~ msgid "Exclude status:"
#~ msgstr "Älä sisällytä tilaa: "

#~ msgid "Include status:"
#~ msgstr "Sisällytä tila: "

#~ msgid "done"
#~ msgstr "valmis"

#~ msgid "fixed"
#~ msgstr "korjattu"

#~ msgid "pending"
#~ msgstr "ratkaisematon"

#~ msgid "forwarded"
#~ msgstr "siirretty eteenpäin"

#~ msgid "open"
#~ msgstr "avoin"

#~ msgid "bugs"
#~ msgstr "viat"

#~ msgid "Distribution:"
#~ msgstr "Jakelu:"

#~ msgid "Package version:"
#~ msgstr "Paketin versio:"

#~ msgid "testing-proposed-updates"
#~ msgstr "testattava - ehdotetut päivitykset"

#~ msgid "proposed-updates"
#~ msgstr "ehdotetut päivitykset"

#~ msgid "don't show statistics in the footer"
#~ msgstr "ei yhteenvetoa alaotsakkeeseen"

#~ msgid "don't show table of contents in the header"
#~ msgstr "ei sisällysluetteloa yläotsakkeeseen"

#~ msgid "no ordering by status or severity"
#~ msgstr "ei järjestystä tilan tai vakavuuden perusteella"

#~ msgid "display merged bugs only once"
#~ msgstr "näytä yhdistetyt viat vain kerran"

#~ msgid "active bugs"
#~ msgstr "aktiiviset viat"

#~ msgid "Flags:"
#~ msgstr "Liput:"
